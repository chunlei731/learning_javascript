var arr = [ 9, -1, 2, 89, 123, -8, 0, 14]
// [ -8, -1, 0, 2, 9, 14, 89, 123]

function bubble_sort(arr){
    for(var end=arr.length-1; end>=0; end--){
        // 找最大的值，放到数组最后
        var index = 0
        for (var i=0; i<=end; i++){
           if ( arr[i] > arr[index]){
               index = i
           }
        }
        // index , end 角标的数要换下位置
        var tmp = arr[end]
        arr[end] = arr[index]
        arr[index] = tmp
    }
    return arr
}

arr = bubble_sort(arr)
console.log(arr)


// 递归函数
// n*logn   mergeSort
// n*logn   quickSort