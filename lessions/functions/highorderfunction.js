function add(x, y, z, f) {
    return f(x) + f(y) + f(z);
}

add(5, 6, 7, Math.sqrt);



var f = function (x) {
    return x * x;
};

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
//[1, 4, 9, 16, 25, 36, 49, 64, 81]

var result = [];
for (var i=0; i<arr.length; i++) {
    result.push((arr[i] * arr[i]));
}




function pow(x) {
    return x * x;
}

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// arr.map(pow); // [1, 4, 9, 16, 25, 36, 49, 64, 81]
arr.map(function (x) {
    return x * x
});


function sum(x , y){
    return x +y
}

var arr = [1, 3, 5, 7, 9];   // 0+ 1 + 3 + 5+ 7 + 9
arr.reduce(sum, 10); // 25



// Array.prototype.filter

function isOdd(x){
    return x %2 !== 0;
}

function isEven(x){
    // return !isOdd(x);
    return x % 2 === 0;
}

var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var r = arr.filter(isOdd);
r; // [1, 5, 9, 15]

var e = arr.filter(isEven);
e

var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var x = Array.prototype.filter.call(arr, isEven);

var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var result = []
for (var elem of arr) {
    if ( elem % 2 !== 0 ){
        result.push(elem);
    }
}
// [1,5,9,15]


function A(){
    var arr = [1, 2, 4, 5, 6, 9, 10, 15];
    var result = []
    for (var elem of arr) {
        if ( elem % 2 !== 0 ){
            return elem;
        }
    }
}

var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); // 注意：IE9以下的版本没有trim()方法
});
arr; // ['A', 'B', 'C']


// Array.prototype.filter
// 自己实现 filter 的功能
var arr = [1, 2, 4, 5, 6, 9, 10, 15];

function Filter1(f){
    var result = []
    for(var elem of this){
        if (f(elem)){
            result.push(elem)
        }
    }
    return result;
}

arr.Filter1 = Filter1

// 自己实现下 map 函数

function Map1(f){
    var res = [];
    for (var elem of this) {
        res.push(f(elem))
    }
    return res;
}

function pow(x) {
    return x * x;
}

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// arr.map(pow); // [1, 4, 9, 16, 25, 36, 49, 64, 81]
arr.map(pow);

arr.Map1 = Map1
arr.Map1(pow)
