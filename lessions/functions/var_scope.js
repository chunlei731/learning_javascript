/**
 * Created by chunleizhang on 31/01/2017.
 */
'use strict';


//
// Local
// external
// global

var x = 3;

//block scope
{
    var y = 1

}



for(var i=0; i<=3; i++){
        console.log("chunlei");

}

function a(){
    var i  = 1;
    return "chunlei";
}

var a = function (){
    var i = 1;
    return "chunlei";
}


{  }

(function (){

    var i = 3;

}())

i

// function expression

var Song = (function () {
    function Song(artist, title) {
        this.artist = artist;
        this.title = title;
    }
    Song.prototype.play = function () {
        console.log('Playing ' + this.title + ' by ' + this.artist);
    };
    Song.Comparer = function (a, b) {
        if (a.title === b.title) {
            return 0;
        }
        return a.title > b.title ? 1 : -1;
    };
    return Song;
}());



console.log(y);  //undefined

function foo() {
    var x = 1;
    function bar() {
        // var x = 5
        console.log(x);
    }
    bar();
}

foo()


