function foo(){
    var rest = [1, 3, 5, 7];
    for (var elem of rest){
        console.log(rest[elem])
    }
}

// rest[1]  = 3
// rest[3]  = 7
// rest[5]  = undefined
// rest[7]  =  undefined
// 1 + 3 + 5 + 7


function sum(...rest){
    var result= 0;
    for (var elem of rest){
        result=rest[elem];
    }
    return result
}
sum(1,3,5,7)


function foo(a, b) {
    var i, rest = [];
    if (arguments.length > 2) {
        for (i = 2; i<arguments.length; i++) {
            rest.push(arguments[i]);
        }
    }
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}

foo("chunlei", "my", "george", "c++", "java");


function foo1(a, b, ...rest) {
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}

foo1(1, 2, 3, 4, 5);
sum(1, 2, 3, 5);  // rest=[1, 2,3,5]
function sum(...rest) {
    var result = 0;
    for( var elem of rest){
        result = result + elem;
    }
    return result;
}

function sum1(...rest){
    var result =0;
    for (var i=0; i< rest.length; i++){
        result = result + rest[i];
    }
    return result;
}

function sum2(...rest){
    var result = 0;
    for (var i in rest){
        result = result + rest[i]
    }
    return result;
}



