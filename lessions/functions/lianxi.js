function a(x){
    if(x>0){
        return x
    }
    return -x;
}
function abs(x){
    if(typeof x !=='number'){
        throw 'Not a numaber'
    }
    if(x>0){
        return x;
    }else{
        return -x;
    }
};

function foo(x){

    for(i=0;i<arguments.length;i++){
        alert(arguments[i]);
    }
}
foo(10,20,30)

function q(x) {
    for(i=0;i<arguments.length;i++){
        alert(arguments[i]);
    }
}
function abs() {
    if(arguments.length===0){
        return 0
    }
    var x =arguments[0];
    if(x>=0){
        return x;
    }
    x<0;
    return -x;
}
//x>=0 ? x:-x;什么意思？

function foo(a,b) {
    var i =[];
    var rest=[];
    if(arguments.length>2){
        for(i=2; i<arguments.length;i++){
            rest.push(arguments[i]);
        }
    }
    console.log('a =' + a);
    console.log('b =' + b);
    console.log(rest);
};

function q(a,b) {
    if(arguments.length>2){
        for(var i=2; i<arguments.length;i++){
            var rest=[];
            rest.push(arguments[i]);
        }
    }
    console.log(a);
    console.log(b);
    console.log(rest);
}

function foo(a,b,...rest) {
    console.log(a);
    console.log(b);
    console.log(rest);
};
function sum(...rest) {
    var s=0;
    for(var i =0;i<rest.length;i++){
               s=s+rest[i]
    }
    return s;
}
//为什么不可以（求和）



function  sum(...rest) {
    var q =0;
    for(var i of rest){
        q=q+i;
    }
    return q;
}
//为什么 var q = 0 不能放在q=q+i;上面  （内部可以访问外部函数定义的变量，反过来则不行；）

function area_of_circle(r,pi) {
    if(arguments.length<2){   //pi==0; / pi='null'为什么不行
         pi =3.14;
    }
    var s=pi*r*r;
    return s;
}

function foo() {
    var x=1;
    function  bar() {
        var y =x+1;
        return y;
    }
    return bar;
}


function foo() {
    console.log('hello_a');
    function foo2() {
        console.log('hello_b');
    }
    return foo2;
    function foo3() {
        console.log('hello_c');
    }
    return foo3;
}

function foo() {
    console.log('hello_a');
    function foo2() {
        console.log('hello_b');
        function foo3() {
            console.log('hello_c');
            function  foo4() {
                console.log('helli_d')
                function  foo5() {
                    console.log('是不是这么套函数啊？')
                }
                return foo5;
            }
            return foo4;
        }
        return foo3;
    }
    return foo2;

};
var a =foo();
var b=a();
var c=b();
var d=c();
var c=d();

function foo() {
    var x=1;
    function  bar() {
        var y = x+1;
        return y;
    }
    return bar;
    var z = bar()+1;
    return z;
}

function foo() {
    var x=1;
    function  bar() {
        var y = x+1;

        function bar2() {
            var z =y+1;
            return z;
        }
        return bar2;
    }
    return bar;
}

function a1() {
    alert('1')
    function a2() {
        alert('2')
        function a3() {
            alert('3')
        }
        return a3;
    }
    return a2;
}

function foo() {
    var x=1;
    function  bar() {
        var y = x+1;

        function bar2() {
            var z =y+1;
            return z;
        }
        return bar2;
    }
    return bar;
}
var a = foo();
var b =a();
var c=b();
var foo=c;

//怎么才能函数运行一次，返回出最后计算完毕的数

function foo() {
    var
        x=1,
        y=2,
        z=3;
    alert(x+''+y+''+z);
}


var cc ={};
cc.name='mingyan';
cc.age=18;
function cc.foo() {
    return 'foo';
}

var cc ={};
cc.name='mingyan';
cc.age=18;
cc.foo=function() {   //function cc.fo为什么不行？
    return 'foo';
}

function foo() {
    for (var i=0; i<100; i++) {
        //
    }
    i += 100; // 仍然可以引用变量i       //什么意思（由于JavaScript的变量作用域实际上是函数内部，我们在for循环等语句块中是无法定义具有局部作用域的变量的：
}

let  ; //只在 for 循环  块级作用域  别的和var一样？

//常量与变量的区别

function a() {
 console.log('a');
    function a2() {
        console.log('b');
        function a3() {
            console.log('c');
        }
        return a3;
 }
    return a2;
}

var xiaoming ={
    name:'小明',
    birth:1990,
    age:function () {
        var y = 2017;
        return y-this.birth;
    }
}

var xiaoming = {
    name:'小明',
    birth:1990,
}
function age1() {
    var y =2017-this.birth;
    return y;
}
age1.call(xiaoming)




function foo() {
    var x=1;
    function  bar() {
        var y = x+1;
    }
    return bar;
    function bar2() {
        var z =foo()()+1;
        return z;
    }
    return bar2;
}

function Cat(name) {
    this.name=name;
    Car.prototype.say=function () {
        return 'Hello'+this.name;
    }
}