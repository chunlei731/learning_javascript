function area_of_circle(r, pi) {

    if (arguments.length === 1) {
        pi = 3.14;
    }

    return pi * r * r;
}