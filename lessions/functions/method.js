/**
 * Created by chunleizhang on 31/01/2017.
 */

var xiaoming = {
    name: '小明',
    birth: 1990
};

var tong = {
    birth: 1980
};

var xiaoming = new Object();
xiaoming.name = 'xiaoming';
xiaoming.birth = 1990


function getAge(msg) {
    console.log(msg);
    var y = new Date().getFullYear();
    return y - this.birth;
}

var xiaoming = {
    name: '小明',
    birth: 1990,
    age: getAge
};



xiaoming.age("hello"); // 27, 正常结果
getAge("hello"); //NaN
getAge.call(tong, "hello");
getAge.call(xiaoming, "hello");

getAge(); // NaN
getAge.call(xiaoming); //27
getAge.call(tong, "hello");
