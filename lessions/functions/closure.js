var xiaoming = {
    name: '小明',
    birth: 1990,
    age: function () {
        var that = this;

        function getAgeFromBirth() {
            var y = new Date().getFullYear();
            return y - that.birth;
        }

        return getAgeFromBirth;
    }
};

var a = xiaoming.age()
var b = a()


var xiaopeng = {
    birth: 2000
};

function age() {
    var that = this

    function getAgeFromBirth() {
        var y = new Date().getFullYear();
        return y - that.birth;
    }

    return getAgeFromBirth;
}

var b = age.call(xiaopeng);

b()


function A() {
    console.log("this is A!");
    function B() {
        console.log("this is B!");
        return "mingyan";
    }

    return B;
}


var a = A();
var b = a(); //B()
A()()


function A() {

    console.log("this is A!");
    console.log("this is B!");
    // return 1;
    return "mingyan";
}


var a = A;
var b = A;
a()
b()
A()


function print(a, b) {
    a()
    b()
}


function A() {

    console.log("this is A");
}
function B() {
    console.log("this is B");
}

print(A, B)


function getAgeFromBirth() {

    return 2017 - this.birth;
}
//NaN
getAgeFromBirth();


var x = 1
// namesapce
//closure
// Local --> external -> global
function A() {
    var x = 2;

    function B() {
        console.log("This is function B!")
        console.log(x);
    }

    return B;
}

var a = A();
a()


Math.max.apply(null, [3, 5, 4]);
Math.max.call(Math, 3, 5, 4);


/*
 1. 基本数据类型
 123, "chunlei", [], {}, Object

 2. 基本语法和控制语句
 var a = 123
 if() { ....}
 for() { ...}

 3. 标准库  (已经实现好的功能）
 Math.max
 Date.getFullYear()

 4. 创建自己的类型和函数
 function Person(name, age){
 this.age = age
 this.name = name
 };

 function print(a, b） {  ..... }

 自己实现的功能，打包成库， 给别人用


 JQuery  YUI(UI Widget) ,  ExJs,  Dojo, Underscore
 */

// function lazy_sum(arr) {
//     var sum = function () {
//         return arr.reduce(function (x, y) {
//             return x + y;
//         });
//     }
//     return sum;
// }


var arr = [1, 2, 3, 4, 5]

var sum = function (x, y) {
    return x + y;
}
arr.reduce(sum)
arr.reduce(sum, 20)


function lazy_sum(arr) {

    function sum(z) {
        return arr.reduce(function (x, y) {
            return x + y;
        }, z);
    }
    return sum
}


var a = lazy_sum([1, 2, 3, 4, 5]); // 1 + 2 + 3 + 4 + 5 = 15
a(20)
