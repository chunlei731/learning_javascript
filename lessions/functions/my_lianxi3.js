var myMath = (function () {

    // var this._wrapped = [];
    function F(obj) {
        if (obj instanceof Array) {
            this._wrapped = obj;
            return this;
        } else {
            console.log("myMath 只接受数组作为参数");
        }
    };

    var _ = F.prototype

    _.min = function () {
        var res = this._wrapped[0];
        for (var elem of this._wrapped) {
            if (res > elem) {
                res = elem;
            }
        }
        return res;
    };

    _.max = function () {
        var res = this._wrapped[0];
        for (var elem of this._wrapped) {
            if (res < elem) {
                res = elem
            }
        }
        return res;
    }

    _.reverse = function () {
        var res = [];
        for (var b = this._wrapped.length - 1; b >= 0; b--) {
            res.push(this._wrapped[b]);
        }
        return res;
    }

    _.average = function () {
        var b = 0;
        for (var a of this._wrapped) {
            b = (b + a);
        }
        return b / this._wrapped.length;
    }

    _.minAndmax = function () {
        var res = []
        var min = this._wrapped[0];
        var max = this._wrapped[0];
        for (var b of this._wrapped) {
            if (min > b) {
                min = b;
            }
            if (max < b) {
                max = b;
            }
        }
        return [min, max];
    }

    return F;
}())


var arr = new myMath([23, 3, -5, 19, 90, -78, 3]);

// console.log(arr)
console.log(arr.max());
console.log(arr.min());
console.log(arr.average());
console.log(arr.minAndmax());


