var myset = (function () {

    function F(){
        this.arr = new Array();
        return this;
    }

    var _ = F.prototype

    _.isInSet = function (elem) {
        for (var e of this.arr) {
            if (e === elem)
                return true;
        }
        return false;
    };

    _.add = function (elem) {
        if (_.isInSet.call(this, elem)) {
            return false;
        }
        this.arr.push(elem)
        return true;
    };

    _.print = function () {
        console.log(this.arr.join(", "));
    };

    _.delete = function (elem){
        var pos = this.arr.indexOf(elem);
        if(pos != -1) {
            this.arr.splice(pos, 1);
        }
    }

    return F;
}());


var ms = new myset();
// console.log(ms);
ms.add(1);
ms.add(2);
ms.add("you win");
ms.add("chunlei");
ms.add("5")
ms.add(3);
ms.add(2);
ms.add("chunlei")
ms.add("5")
ms.print();

ms.delete(1);
ms.delete(2);
ms.print();