var myMath = (function () {

    var arr = [];
    var _ = function (obj) {
        if (obj instanceof Array) {
            arr = obj;
            return _;
        } else {
            console.log("myMath 只接受数组作为参数");
        }
    };

    _.min = function () {
        var res = arr[0];
        for (var elem of arr) {
            if (res > elem) {
                res = elem;
            }
        }
        return res;
    };

    _.max = function () {
        var res = arr[0];
        for (var elem of arr) {
            if (res < elem) {
                res = elem
            }
        }
        return res;
    }

    _.reverse = function () {
        var res = [];
        for (var b = arr.length - 1; b >= 0; b--) {
            res.push(arr[b]);
        }
        return res;
    }

    _.average = function () {
        var b = 0;
        for (var a of arr) {
            b = (b + a);
        }
        return b / arr.length;
    }

    _.minAndmax = function () {
        var res = []
        var min = arr[0];
        var max = arr[0];
        for (var b of arr) {
            if (min > b) {
                min = b;
            }
            if (max < b) {
                max = b;
            }
        }
        return [min, max];
    }

    return _;
}())


var arr = myMath([23, 3, -5, 19, 90, -78, 3]);

// console.log(arr)
console.log(arr.max());
console.log(arr.min());
console.log(arr.average());
console.log(arr.minAndmax());


