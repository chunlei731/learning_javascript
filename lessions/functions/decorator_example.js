
var times = 0;
function do_work(){
    times = times +1;
    console.log("Times: " + times);
    console.log("do work");
}



function do_work(){
    console.log("do work");
}
var f = do_work;
f();


// 装饰器： 在原来的功能上， 增加额外的功能， 但是不改变原有函数。

function times_decorator(f) {
    var times = 0;
    function foo() {
        times = times + 1;
        console.log("Times: " + times);
        f();
    }
    return foo;
}

do_work = times_decorator(do_work);
do_work();

a= times_decorator(do_work);
a();

function aa(f) {
    var a = new Date();
    var b= new Date();
    console.log(b-a);
    f();
};



function aa(f) {
    var a = new Date();
    function foo1() {
        b= new Date();
        console.log(b-a);
        //f.apply(null,this.arguments);
        f();
    }
    return foo1;
};


function isPrime(x){
    //2, 3, 4, 5, 6, 7, 8, 9,10 , 39
    //77
    if (x === 1) return false;
    if (x === 2) return true;

    var y = Math.ceil(Math.sqrt(x));

    for (var i =2; i <= x/2; i++){
        if (x % i === 0){
            return false;
        }
    }

    return true;

}

isPrime(827263784907);

zhi = aa(isPrime);
zhi()

//写一个装饰器， 调用 isPrime() 的时候， 不但能告诉我，这个数是不是质数，还能告诉， isPrime() 这个函数运行了多长时间。
// var a = new Date(),    b= new Date(),   b - a



function times(f) {
    var times = 0;

    function foo() {
        times = times + 1;
        console.log("Times: " + times)
        f.apply(null, arguments)
       //  f();

    }

    return foo;
}

function do_work(){
    console.log(" i am doing work");
    console.log(Array.prototype.slice.call(arguments))
}

do_work = times(do_work)
do_work("chunlei", 1, 2);
// foo("chunlei, 1,2 );


function aa(f) {
    var a = new Date();
    function foo1() {
        b= new Date();
        console.log(b-a);
        var result = f.apply(null, arguments);
        // f();
        return result
    }
    return foo1;
};

zhi = aa(isPrime);
zhi(1234567)





function sleep(milliSeconds){
    var startTime = new Date().getTime(); // get the current time
    while (new Date().getTime() < startTime + milliSeconds); // hog cpu
}

function isPrime(x){

    sleep(1200);
    //2, 3, 4, 5, 6, 7, 8, 9,10 , 39
    //77
    if (x === 1) return false;
    if (x === 2) return true;

    var y = Math.ceil(Math.sqrt(x));

    for (var i =2; i <= x/2; i++){
        if (x % i === 0){
            return false;
        }
    }

    return true;

}


function aa(f){

    function foo(){
        var a = new Date();

        // f();
        var result = f.apply(null, arguments);

        var b = new Date();
        console.log("spend time: " + (b - a));
        return result;
    }

    return foo;

}

zhi = aa(isPrime);
zhi(123456789)


function f(a, b, c){
    console.log(a, b, c)
}

f( 1, 2, 3)
f("a", "b", "c")

var x = [1, 2, 3]
f(x)
f.apply(null, x)
