//Array.prototype

var a = [1,2,3]  // var a = new Array(1,2,3);

a.slice(1);
Array.prototype.slice.call(a, 1);


a.__proto__ = null;
// slice 再也用不了了。
a.slice(1);


// [ "chunlei", "mingyan"]
var b = new Array("chunlei", "mingyan")





function  foo(){

}

foo.prototype.constructor  === foo





var Student = {
    name: 'Robot',
    height: 1.2,
    run: function () {
        console.log(this.name + ' is running...');
    }
};

var xiaoming = {
    name: '小明'
};
//var xiaoming = new Object（{ name: 'xiaoming'})

xiaoming.__proto__ = Student;



xiaoming.run()



// 原型对象:
var Student = {
    name: 'Robot',
    height: 1.2,
    run: function () {
        console.log(this.name + ' is running...');
    }
};

var xiaoming = Object.create(Student);
xiaoming.name = "xiaoming"
xiaoming.run()

// var xiaoming = new Object();
// xiaoming.__proto__ = Student



// 构造函数

function Student(name) {
    this.name = name;
    this.hello = function () {
        console.log('Hello, ' + this.name + '!');
    }
}

var a = new Student("chunlei");
var b = new Student("mingyan");




function Student(name) {
    this.name = name;
}

Student.prototype.classname = "二年一班";
Student.prototype.hello = function () {
    console.log('Hello, ' + this.name + '!');
};

var a = new Student("chunlei");
var b = new Student("mingyan");


var a = new Object();
var b = new Object();
b.__proto__ = a;


function A(name){
    this.name = name
}

var s = new A()


// 原型对象:
var Student = {
    name: 'Robot',
    height: 1.2,
    run: function () {
        console.log(this.name + ' is running...');
    }
};


// 原型对象:
var Student = new Object();
Student.name = 'Robot';
Student.height = 1.2;
Student.run = function(){
    console.log(this.name + ' is running...');
}

// factory method  工厂函数
function createStudent(name) {
    // 基于Student原型创建一个新对象:
    var s = Object.create(Student);
    // var s = new Object();
    // s.__proto__ = Student;

    // 初始化新对象:
    s.name = name;
    return s;
}

var xiaoming = createStudent('小明');
var cl = createStudent("chunlei");

xiaoming.run(); // 小明 is running...
xiaoming.__proto__ === Student; // true
xiaoming.__proto__ === Object.prototype //false




