function Student(props) {
    this.name = props.name || 'Unnamed';
}

Student.prototype.hello = function(){
    console.log('Hello, ' + this.name + '!');
}

function PrimaryStudent(props) {
    Student.call(this, props);
    this.grade = props.grade || 1;
}

function inherits(Child, Parent) {
    var F = function () {};
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
}

inherits(PrimaryStudent, Student);

PrimaryStudent.prototype.getGrade = function () {
    return this.grade
};

var xiaoming = new PrimaryStudent({name: "xiaoming", grade: 94});
xiaoming.hello();
var g = xiaoming.getGrade()
console.log(g);


var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };


function inherits(Child, Parent) {
    var F = function () {};
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
}



function inherits(Child, Parent){

    // function F(){}
    // F.prototype = Parent.prototype
    // Child.prototype = new F();
    // Child.prototype.constructor = Child;

    Child.prototype = Object.create(Parent.prototype)
    Child.prototype.constructor = Child

}
