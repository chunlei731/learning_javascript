

function Student(props) {
    this.name = props.name || 'Unnamed';
}

Student.prototype.hello = function(){
    console.log('Hello, ' + this.name + '!');
}

function PrimaryStudent(props) {
    Student.call(this, props);
    this.grade = props.grade || 1;
}

PrimaryStudent.prototype.getGrade = function () {
    return this.grade
};

// 这种方式，在 IE 或者有些浏览器，是不能工作的。
PrimaryStudent.prototype.__proto__ = Student.prototype

var xiaoming = new PrimaryStudent({name: "xiaoming", grade: 94});
xiaoming.hello();
var g = xiaoming.getGrade()
console.log(g);


/*
1. 不能用  __proto__
2. Function.prototype  可以用
3. Object.create()  可以用

 */

Array.prototype.constructor === Array  //true

var c = []
var b = new Array();
var a = Object.create(Array.prototype)
