// 为了运行下面的脚本，你需要在 cmd.exe 下运行
// npm install underscore
var _ = require('underscore')


var res = _.map({one: 1, two: 2, three: 3}, function(num, key){ return num * 3; });
console.log(res);


var list = [[0, 1], [2, 3], [4, 5]];
var flat = _.reduce(list, function(a, b) { return a.concat(b); }, []);
console.log(flat)

// 注意和上个例子的区别
var list = [[0, 1], [2, 3], [4, 5]];
var flat = _.reduceRight(list, function(a, b) { return a.concat(b); }, []);
console.log(flat)


// _.where(listOfPlays, {author: "Shakespeare", year: 1611});

var stooges = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}];
_.max(stooges, function(stooge){ return stooge.age; });


var res = _.sortBy([1, 2, 3, 4, 5, 6], function(num){ return Math.sin(num); });
console.log(res)

var res = (function(){ return _.toArray(arguments).slice(1); })(1, 2, 3, 4);
console.log(res)


function isOdd(x){
    return x % 2 != 0;
}
// var a, b = _.partition([0, 1, 2, 3, 4, 5], isOdd);
var a  = _.partition([0, 1, 2, 3, 4, 5], x => x %2 !=0);
console.log(a[0], a[1])


var res = _.last([5, 4, 3, 2, 1]);
console.log(res);

